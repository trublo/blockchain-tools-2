require("dotenv").config();
// const HDWalletProvider = require("truffle-hdwallet-provider");
const HDWalletProvider = require("@truffle/hdwallet-provider");
const mnemonic = process.env.APP_MNEMONIC;
const projectId = process.env.APP_PROJECT_ID;

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" // "*" Match any network id
    },
    mainnet: {
      provider: new HDWalletProvider({
        mnemonic,
        providerOrUrl: `https://mainnet.infura.io/v3/${projectId}`
      }),
      network_id: 1
    },
    ropsten: {
      provider: new HDWalletProvider({
        mnemonic,
        providerOrUrl: `https://ropsten.infura.io/v3/${projectId}`
      }),
      network_id: 3
    },
    kovan: {
      provider: new HDWalletProvider({
        mnemonic,
        providerOrUrl: `https://kovan.infura.io/v3/${projectId}`
      }),
      network_id: 42
    },
    rinkeby: {
      provider: new HDWalletProvider({
        mnemonic,
        providerOrUrl: `https://rinkeby.infura.io/v3/${projectId}`
      }),
      network_id: 4
    },
    goerli: {
      provider: new HDWalletProvider({
        mnemonic,
        providerOrUrl: `https://goerli.infura.io/v3/${projectId}`
      }),
      network_id: 5
    },
    alastriaQuorum: {
      provider: new HDWalletProvider({ providerOrUrl: "https://baas.trublo.eu/rpc/quorum/tquorumatc", mnemonic }),
      network_id: "*",
      type: "quorum",
      gasPrice: 0
    },
    alastriaBesu: {
      provider: new HDWalletProvider({ providerOrUrl: "https://baas.trublo.eu/rpc/besu/tbesuatc", mnemonic }),
      network_id: "*",
      type: "quorum",
      gasPrice: 0
    },
    besu: {
      provider: new HDWalletProvider({
        providerOrUrl: "http://127.0.0.1:8545",
        // Testing accounts with private keys
        privateKeys: [
          "0x8f2a55949038a9610f50fb23b5883af3b4ecb3c3bb792cbcefbd1542c692be63",
          "0xc87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3",
          "0xae6ae8e5ccbfb04590405997ee2d52d2b330726137b875053c36d94e974d162f"
        ],
        addressIndex: 0,
        numberOfAddresses: 3
      }),
      network_id: "*",
      gasPrice: 0
    }
  },
  compilers: {
    solc: {
      version: "0.8.3",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      },
      evmVersion: "byzantium"
    }
  }
};

// 0.5.7
